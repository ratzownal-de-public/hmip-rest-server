const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors')
const axios = require('axios')
const fs = require('fs');
const os = require('os');
const { time } = require('console');

const app = express();
app.use(cors())

// Set up Global configuration access
dotenv.config();

//load config
if (!fs.existsSync("config.json")) {
    console.error("config.json not found! Please run installer.js first!");
    return;
}
const configJson = JSON.parse(fs.readFileSync('config.json'));
const AUTH_TOKEN = configJson.auth_token;
const CLIENT_ID = configJson.client_id;
const CLIENT_AUTH = configJson.client_auth;
const REST_URL = configJson.rest_url;

if (!AUTH_TOKEN || !CLIENT_ID || !CLIENT_AUTH || !REST_URL) {
    console.error("config.json is broken. Please run installer.js again!");
    return;
}

const data = {
    "clientCharacteristics": {
        "apiVersion": "12",
        "applicationIdentifier": "hmip-server",
        "applicationVersion": "1.0",
        "deviceManufacturer": "none",
        "deviceType": "Computer",
        "language": "en",
        "osType": os.type(),
        "osVersion": os.version()
    }
}

const headers = {
    "VERSION": "12",
    "Content-Type": "application/json",
    "CLIENTAUTH": CLIENT_AUTH,
    "AUTHTOKEN": AUTH_TOKEN
}

let PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server is up and running on ${PORT} ...`);
});

app.use(function (req, res, next) {
    console.log(Date.now() + ' - Request URL:', req.originalUrl);
    console.log(Date.now() + ' - Request Type:', req.method);
    next();
});

app.get('/get-state', async (req, res) => {
    const response = await getState();
    res.json(response.data);
});

//example slice devices
app.get('/get-devices', async (req, res) => {
    const response = await getState();
    res.json(response.data.devices);
});

async function getState() {
    return axios.default.post(REST_URL + "/hmip/home/getCurrentState", data, { headers: headers });
}