# Installation

Clone the repository and run "npm install"

Run installer.js ("node installer.js") and enter your settings like SGTIN, PIN and the user (device) you want to name your API.

```
//EDIT your settings!
const ID = "xxx"; // SGTIN
const PIN = "xxx"; // PIN of access point
const DEVICE_NAME = "my-api-user"; // this name will be displayed as connected user
```

If everything is completed there should be a config.json file in the folder. Now run "node index.js".

The server should be running fine now and you should be able to access it via

http://localhost:4000/get-state

You will get a json response with all information of your Homematic IP environment.

# Used environment

node -v -> v18.12.1

npm -v -> 9.2.0