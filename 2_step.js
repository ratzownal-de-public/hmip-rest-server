const axios = require('axios');
const crypto = require('crypto');
const HASH = crypto.createHash('sha512');

//EDIT your settings!
const ID = "3014F211A000215BE9A5ABCD";
const PIN = "1234";
const DEVICE_NAME = "my-api-user";
const REST_URL = "https://srz36.homematic.com:6969";

const UUID = crypto.randomUUID();
console.log("Used UUID=" + UUID);
const CLIENTAUTH = HASH.update(ID + 'jiLpVitHvWnIGD1yo7MA', 'utf-8').digest('hex').toUpperCase();

var data = { "deviceId": UUID, "deviceName": DEVICE_NAME, "sgtin": ID }
var headers = {
    "content-type": "application/json",
    "accept": "application/json",
    "VERSION": "12",
    "CLIENTAUTH": CLIENTAUTH,
    "PIN": PIN
}

var main = (async () => {
    console.log("Sending connection request ...");
    try {
        response = await axios.default.post(REST_URL + "/hmip/auth/connectionRequest", data, { headers: headers });
    } catch (error) {
        console.error(error);
        return;
    }
    console.log("Connection request was successfully sent!");
})();