const axios = require('axios');
const os = require('os');

//EDIT your settings!
const ID = "3014F211A000215BE9A5ABCD";

const LOOKUP_URL = "https://lookup.homematic.com:48335/getHost";
const LOOKUP_JSON = {
    "clientCharacteristics": {
        "apiVersion": "12",
        "applicationIdentifier": "hmip-server",
        "applicationVersion": "1.0",
        "deviceManufacturer": "none",
        "deviceType": "Computer",
        "language": "en",
        "osType": os.type(),
        "osVersion": os.release()
    },
    "id": ID
}

var main = (async () => {
    //getting your rest url
    console.log("Starting getting your rest url...")
    var response = null;
    try {
        response = await axios.default.post(LOOKUP_URL, LOOKUP_JSON);
        console.log(response.data.urlREST);
    } catch (error) {
        console.error("Something went wrong! Maybe your SGTIN is incorrect!");
        console.error(error.response.data);
        return;
    }
})();