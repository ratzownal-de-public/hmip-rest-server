const axios = require('axios');
const crypto = require('crypto');
const HASH = crypto.createHash('sha512');

const ID = "3014F211A000215BE9A5ABCD";
const PIN = "1234";
const UUID = "UUID-FROM-LAST-STEP";
const REST_URL = "https://srz36.homematic.com:6969";
const AUTH_TOKEN = "AUTH-TOKEN-LAST-STEP";

const CLIENTAUTH = HASH.update(ID + 'jiLpVitHvWnIGD1yo7MA', 'utf-8').digest('hex').toUpperCase();

var data = { "deviceId": UUID }
var headers = {
    "content-type": "application/json",
    "accept": "application/json",
    "VERSION": "12",
    "CLIENTAUTH": CLIENTAUTH,
    "PIN": PIN
}

var main = (async () => {
    //confirm authToken!
    console.log("Confirming authToken ...");
    var data = { "deviceId": UUID, "authToken": AUTH_TOKEN }
    try {
        response = await axios.default.post(REST_URL + "/hmip/auth/confirmAuthToken", data, { headers: headers });
    } catch (error) {
        handleError(error);
        return;
    }
    const clientId = response.data.clientId;
    console.log("clientId="+clientId);
})();