const axios = require('axios');
const crypto = require('crypto');
const os = require('os');
const HASH = crypto.createHash('sha512');

//EDIT your settings!
const ID = "3014F211A000215BE9A5ABCD";
const REST_URL = "https://srz36.homematic.com:6969";
const AUTH_TOKEN = "YOUR-AUTH-TOKEN";
const CLIENT_AUTH = HASH.update(ID + 'jiLpVitHvWnIGD1yo7MA', 'utf-8').digest('hex').toUpperCase();

const data = {
    "clientCharacteristics": {
        "apiVersion": "12",
        "applicationIdentifier": "hmip-server",
        "applicationVersion": "1.0",
        "deviceManufacturer": "none",
        "deviceType": "Computer",
        "language": "en",
        "osType": os.type(),
        "osVersion": os.version()
    }
}

const headers = {
    "VERSION": "12",
    "Content-Type": "application/json",
    "CLIENTAUTH": CLIENT_AUTH,
    "AUTHTOKEN": AUTH_TOKEN
}

var main = (async () => {
    try {
        const response = await axios.default.post(REST_URL + "/hmip/home/getCurrentState", data, { headers: headers });
        for(const [deviceId, device] of Object.entries(response.data.devices)){
            if(device.type == "HEATING_THERMOSTAT"){
                console.log(device.functionalChannels['1']);
            }
        }
    } catch (error) {
        console.error(error);
        return;
    }
})();