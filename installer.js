const axios = require('axios');
const crypto = require('crypto');
const fs = require('fs');
const os = require('os');
const envfile = require('envfile');
const HASH = crypto.createHash('sha512');

//EDIT your settings!
const ID = "xxx";
const PIN = "xxx";
const DEVICE_NAME = "my-api-user";

const LOOKUP_URL = "https://lookup.homematic.com:48335/getHost";
const LOOKUP_JSON = {
    "clientCharacteristics": {
        "apiVersion": "12",
        "applicationIdentifier": "hmip-server",
        "applicationVersion": "1.0",
        "deviceManufacturer": "none",
        "deviceType": "Computer",
        "language": "en",
        "osType": os.type(),
        "osVersion": os.release()
    },
    "id": ID
}

const UUID = crypto.randomUUID();
const CLIENTAUTH = HASH.update(ID + 'jiLpVitHvWnIGD1yo7MA', 'utf-8').digest('hex').toUpperCase();

function handleError(error) {
    console.error(error.response.data);
}

(async () => {
    //getting your rest url
    console.log("Starting getting your rest url...")
    var response = null;
    try {
        response = await axios.default.post(LOOKUP_URL, LOOKUP_JSON);
    } catch (error) {
        console.error("Something went wrong! Maybe your SGTIN is incorrect!");
        handleError(error);
        return;
    }

    //storing rest url
    const restURL = response.data.urlREST;
    console.log("RestURL=" + restURL);

    //connection request
    var data = { "deviceId": UUID, "deviceName": DEVICE_NAME, "sgtin": ID }
    var headers = {
        "content-type": "application/json",
        "accept": "application/json",
        "VERSION": "12",
        "CLIENTAUTH": CLIENTAUTH,
        "PIN": PIN
    }

    console.log("Sending connection request ...");
    try {
        response = await axios.default.post(restURL + "/hmip/auth/connectionRequest", data, { headers: headers });
    } catch (error) {
        handleError(error);
        return;
    }
    console.log("Connection request was successfully sent!");

    //you need to press your blue button on your access point now!
    statusCode = 0;
    var data = { "deviceId": UUID }

    while (statusCode != 200) {
        console.log("Please press your blue button on the access point!")
        try {
            response = await axios.default.post(restURL + "/hmip/auth/isRequestAcknowledged", data, { headers: headers });
            statusCode = response.status;
        } catch (error) {
            //silence
        }
        await new Promise(resolve => setTimeout(resolve, 1000));
    }

    console.log("Button press recordnized!");

    //get request authToken!
    console.log("Getting authToken ...");
    try {
        response = await axios.default.post(restURL + "/hmip/auth/requestAuthToken", data, { headers: headers });
    } catch (error) {
        handleError(error);
        return;
    }
    const authToken = response.data.authToken;

    //confirm authToken!
    console.log("Confirming authToken ...");
    var data = { "deviceId": UUID, "authToken": authToken }
    try {
        response = await axios.default.post(restURL + "/hmip/auth/confirmAuthToken", data, { headers: headers });
    } catch (error) {
        handleError(error);
        return;
    }
    const clientId = response.data.clientId;

    console.log("Configuration successfully completed. Writing config.json file...");

    const config = {
        "client_id": clientId,
        "auth_token": authToken,
        "client_auth": CLIENTAUTH,
        "rest_url" : restURL
    }

    fs.writeFileSync("config.json", JSON.stringify(config, null, 2));

})();