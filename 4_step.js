const axios = require('axios');
const crypto = require('crypto');
const HASH = crypto.createHash('sha512');

const ID = "3014F211A000215BE9A5ABCD";
const PIN = "1234";
const UUID = "UUID-FROM-LAST-STEP";
const REST_URL = "https://srz36.homematic.com:6969";

const CLIENTAUTH = HASH.update(ID + 'jiLpVitHvWnIGD1yo7MA', 'utf-8').digest('hex').toUpperCase();

var data = { "deviceId": UUID }
var headers = {
    "content-type": "application/json",
    "accept": "application/json",
    "VERSION": "12",
    "CLIENTAUTH": CLIENTAUTH,
    "PIN": PIN
}

var main = (async () => {
    //get request authToken!
    console.log("Getting authToken ...");
    try {
        response = await axios.default.post(REST_URL + "/hmip/auth/requestAuthToken", data, { headers: headers });
    } catch (error) {
        console.error(error.response.data);
        return;
    }
    const authToken = response.data.authToken;
    console.log("authToken=" + authToken);
})();